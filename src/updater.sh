#!/usr/bin/env bash
set -e


###########################################################################
# Global Constants / Variables
###########################################################################
# These variables are used by their corresponding functions
WHICH_FIND_RESULT=""
WHICH_FIND_STATUS=0

# Configuration Constants (determined by script)
# The package manager used by the system
PACKAGE_MANAGERS=("zypper" "pacman" "apt")
PACKAGE_MANAGER=""
# The utility to use to download web resources
WEB_FETCHERS=("curl" "wget")
WEB_FETCHER=""
# Other
APP_USER_NAME=""   # For non-root commands, use this user
APP_USER_GROUP=""  # For non-root commands, use this group
APP_USER_HOME=""   # For non-root commands, use this home dir

# Constants
GIT_BASE_URL="https://gitlab.com/joeysbytes"


###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    local updater_arguments=("$@")
    print_heading "Initialization"
    check_if_root
    process_updater_arguments "${updater_arguments[@]}"
    find_package_manager
    find_web_fetcher

    print_heading "Update System Packages"
    eval "update_system_with_${PACKAGE_MANAGER}"

    update_bash_aliases_config
    update_nano_config

    update_miniconda3_base

    # TODO: Go
    # TODO: Docker
    # TODO: Reboot
    # TODO: VirtualBox
    # TODO: PiHole
}


###########################################################################
# Initialization
###########################################################################

#--------------------------------------------------------------------------
# find_package_manager:
#   Find the command to use for package management.
#
# Parameters: None
# Output:     Sets PACKAGE_MANAGER
#--------------------------------------------------------------------------
function find_package_manager() {
    which_find_array "${PACKAGE_MANAGERS[@]}"
    if [ "${WHICH_FIND_STATUS}" -ne 0 ]
    then
        echo "ERROR: Cannot find package manager">&2
        exit 1
    fi
    PACKAGE_MANAGER=$(basename "${WHICH_FIND_RESULT}")
    echo "Package Manager: ${PACKAGE_MANAGER}"
}


#--------------------------------------------------------------------------
# find_web_fetcher:
#   Find the command to use when downloading resources (curl or wget).
#
# Parameters: None
# Output:     Sets WEB_FETCHER to command to use
#--------------------------------------------------------------------------
function find_web_fetcher() {
    which_find_array "${WEB_FETCHERS[@]}"
    if [ "${WHICH_FIND_STATUS}" -ne 0 ]
    then
        echo "ERROR: Cannot find web fetcher (curl or wget)">&2
        exit 1
    fi
    WEB_FETCHER=$(basename "${WHICH_FIND_RESULT}")
    echo "Web Fetcher: ${WEB_FETCHER}"
}


#--------------------------------------------------------------------------
# process_updater_arguments:
#   Process the arguments passed in to the updater script.
#
# Parameters:
#   1 - array of all parameters passed in to the updater script
#     1 - user of system for non-root commands
#
# Output: Sets APP_USER_NAME, APP_USER_GROUP, APP_USER_HOME
#--------------------------------------------------------------------------
function process_updater_arguments() {
    local updater_arguments=("$@")
    if [ "${#updater_arguments[@]}" -ne 1 ]
    then
        echo "Usage: updater.sh appUser"
        echo ""
        echo "Parameters given: ${updater_arguments[*]}"
        exit 1
    fi
    # get app user
    APP_USER_NAME="${updater_arguments[0]}"
    set +e
    id -u "${APP_USER_NAME}">/dev/null 2>&1
    local user_id_status=$?
    set -e
    if [ "${user_id_status}" -ne 0 ]
    then
        echo "ERROR: App User not found: ${APP_USER_NAME}">&2
        exit 1
    fi
    echo "App User: ${APP_USER_NAME}"
    # get app user primary group
    APP_USER_GROUP=$(id -gn "${APP_USER_NAME}")
    echo "App User Group: ${APP_USER_GROUP}"
    # get app user home directory
    APP_USER_HOME=$(getent passwd "${APP_USER_NAME}"|cut -f6 -d:)
    echo "App User Home Dir: ${APP_USER_HOME}"
}


#--------------------------------------------------------------------------
# check_if_root:
#   Make sure this script is running as the root user.
#
# Parameters: None
# Output:     None
#--------------------------------------------------------------------------
function check_if_root() {
    local me
    me=$(whoami)
    if [ ! "${me}" == "root" ]
    then
        echo "ERROR: Not running as root: ${me}"
        exit 1
    fi
    echo "Running as root user: True"
}


###########################################################################
# Update System Packages
###########################################################################

#--------------------------------------------------------------------------
# update_system_with_packageManager:
#   These functions update the system with the appropriate package manager
#
# Parameters: None
# Output:     None
#--------------------------------------------------------------------------

function update_system_with_zypper() {
    set +e
    zypper refresh
    zypper update \
      --no-confirm \
      --auto-agree-with-licenses
    set -e
}


###########################################################################
# Update Config Files
###########################################################################

#--------------------------------------------------------------------------
# update_bash_aliases_config:
#   If bash is installed, download the .bash_aliases script and install it
#   for the app user.
#
# Parameters: None
# Output:     None
#--------------------------------------------------------------------------
function update_bash_aliases_config() {
    which_find "bash"
    if [ "${WHICH_FIND_STATUS}" -eq 0 ]
    then
        print_heading "Update bash aliases config"
        local orig_bash_aliases_file="${APP_USER_HOME}/.bash_aliases"
        if [ ! -f "${orig_bash_aliases_file}" ]
        then
            touch "${orig_bash_aliases_file}"
        fi
        local bash_aliases_file
        bash_aliases_file=$(realpath "${orig_bash_aliases_file}")
        local bash_aliases_url="${GIT_BASE_URL}/config-files/-/raw/main/bash/.bash_aliases"
        download_file "${bash_aliases_url}" "${bash_aliases_file}"
        chown "${APP_USER_NAME}":"${APP_USER_GROUP}" "${bash_aliases_file}"
        chmod 644 "${bash_aliases_file}"
        ls -l "${bash_aliases_file}"
    fi
}


#--------------------------------------------------------------------------
# update_nano_config:
#   If nano is installed, determines the major version and downloads /
#   updates the .nanorc file for the app user.
#
# Parameters: None
# Output:     None
#--------------------------------------------------------------------------
function update_nano_config() {
    local nano_major_version="0"
    which_find "nano"
    if [ "${WHICH_FIND_STATUS}" -eq 0 ]
    then
        print_heading "Update nano config"
        local orig_nanorc_file="${APP_USER_HOME}/.nanorc"
        if [ ! -f "${orig_nanorc_file}" ]
        then
            touch "${orig_nanorc_file}"
        fi
        nano_major_version=$(nano --version|head -1|cut -f5 -d\ |cut -f1 -d.)
        echo "Nano Major Version: ${nano_major_version}"
        local nanorc_file
        nanorc_file=$(realpath "${orig_nanorc_file}")
        local nanorc_url="${GIT_BASE_URL}/config-files/-/raw/main/nano/.nanorc${nano_major_version}"
        download_file "${nanorc_url}" "${nanorc_file}"
        chown "${APP_USER_NAME}":"${APP_USER_GROUP}" "${nanorc_file}"
        chmod 644 "${nanorc_file}"
        ls -l "${nanorc_file}"
    fi
}


###########################################################################
# Specific Applications
###########################################################################

#--------------------------------------------------------------------------
# update_miniconda3_base:
#   If Miniconda 3 is installed, update the base environment.
#
# Parameters: None
# Output:     None
#--------------------------------------------------------------------------
function update_miniconda3_base() {
    local miniconda_base_dir="/work/miniconda3"
    if [ -d "${miniconda_base_dir}" ]
    then
        print_heading "Update Miniconda 3"
        local miniconda_bin_dir="${miniconda_base_dir}/bin"
        cd "${miniconda_bin_dir}"
        # shellcheck disable=SC1091
        source ./activate base
        conda update --yes conda
        conda update --yes --all
        conda deactivate
    fi
}


###########################################################################
# Utilities
###########################################################################

#--------------------------------------------------------------------------
# which_find:
#   Given a command string, perform a which against it to see if the
#   software exists on the path.
#
# Parameters:
#   1 - utility / command to find
#
# Output: Sets WHICH_FIND_RESULT, WHICH_FIND_STATUS
#--------------------------------------------------------------------------
function which_find() {
    local to_find="${1}"
    set +e
    WHICH_FIND_RESULT=$(which "${to_find}" 2>&1)
    WHICH_FIND_STATUS=$?
    set -e
}


#--------------------------------------------------------------------------
# which_find_array:
#   Given an array of string commands, perform a which against them until
#   the first match is found on the path (if any).
#
# Parameters:
#   1 - Array of utilities / commands to find
#
# Output: Sets WHICH_FIND_RESULT, WHICH_FIND_STATUS
#--------------------------------------------------------------------------
function which_find_array() {
    local array_to_find=("$@")
    local to_find
    for to_find in "${array_to_find[@]}"
    do
        which_find "${to_find}"
        if [ "${WHICH_FIND_STATUS}" -eq 0 ]
        then
            break
        fi
    done
}


#--------------------------------------------------------------------------
# download_file:
#   Given a url and absolute file path, download a file and save as said
#   full file path.
#
# Parameters:
#   1 - URL of file to download
#   2 - Absolute path file name to save as
#
# Output: File is downloaded and saved to absolute path name.
#--------------------------------------------------------------------------
function download_file() {
    local url="${1}"
    local file_name="${2}"
    echo "Downloading: ${url}"
    echo "  To: ${file_name}"
    if [ "${WEB_FETCHER}" == "curl" ]
    then
        curl "${url}" --output "${file_name}"
    elif [ "${WEB_FETCHER}" == "wget" ]
    then
        wget -O "${file_name}" "${url}"
    fi
    echo "Download Complete"
}


#--------------------------------------------------------------------------
# print_heading:
#   Given a text line, output as a formatted heading.
#
# Parameters:
#   1 - Heading text
#
# Output: Prints the heading
#--------------------------------------------------------------------------
function print_heading() {
    local heading_text="${1}"
    echo ""
    echo "==========================================================================="
    echo "${heading_text}"
    echo "==========================================================================="
}


###########################################################################
# Begin Script
###########################################################################
main "$@"
