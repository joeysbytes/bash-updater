# Bash Updater

Universal bash update script to handle a variety of Linux distributions and specific software packages.

This script grows / adjusts as my personal needs dictate. At this time, I do not have the intentions to try and come
up with code for all scenarios for all popular distributions and software.
